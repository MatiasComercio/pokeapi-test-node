module.exports = {
  verbose: true,
  testRegex: '/tests/.*/.*\\.test.*',
  setupFilesAfterEnv: ['./tests/jest.setup.js']
};
