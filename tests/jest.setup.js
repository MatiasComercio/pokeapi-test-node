const knex = require('../knex');

// Thanks: https://github.com/knex/knex/issues/2686#issuecomment-545210690
afterAll(async done => {
  await knex.destroy();
  done();
});
