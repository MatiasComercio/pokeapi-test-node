const middlewareLogger = require('express-wolox-logger').expressMiddleware;
const express = require('express');
const cors = require('cors');
const path = require('path');
const config = require('./config');
const routes = require('./app/routes');
const logger = require('./app/logger');
const errorsMiddleware = require('./app/middlewares/errors');

const start = () => {
  const app = express();
  const { port } = config;

  app.use(middlewareLogger({ loggerFn: logger.info }));

  // enable CORS
  app.use(cors());

  // set path to serve static files
  app.use(express.static(path.join(__dirname, 'public')));

  routes.load(app);

  app.use(errorsMiddleware.handle);

  app.listen(port, () => logger.info(`Listening on port: ${port}`));
};

start();
