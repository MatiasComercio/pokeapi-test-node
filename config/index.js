const _ = require('lodash');
const dotenv = require('dotenv');

const ENVIRONMENT = process.env.NODE_ENV || 'development';
const envConfig = require(`./${ENVIRONMENT}`);

if (ENVIRONMENT !== 'production') {
  dotenv.config();
}

const defaultConfig = {
  environment: ENVIRONMENT,
  elasticsearch: {
    host: process.env.ES_HOST,
    logLevel: process.env.ES_LOG_LEVEL,
    version: process.env.ES_VERSION
  },
  headers: {
    apiKey: 'x-api-key',
    apiKeyValue: process.env.API_KEY || 'apiKey'
  },
  port: process.env.PORT || 3000
};

module.exports = _.merge(defaultConfig, envConfig);
