const config = {
  environment: 'test',
  database: {
    name: process.env.DB_TEST_NAME
  }
};

module.exports = config;
