const elasticsearch = require('elasticsearch');
const esConfig = require('./config').elasticsearch;

const client = new elasticsearch.Client({
  host: esConfig.host,
  log: esConfig.logLevel,
  apiVersion: esConfig.version
});

module.exports = client;
