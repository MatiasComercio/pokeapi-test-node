FROM node:12.13.0

ARG port

WORKDIR /app

COPY package.json .
COPY package-lock.json .
COPY .nvmrc .

RUN npm install

COPY . .

EXPOSE $port

CMD ["node", "app.js"]