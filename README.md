# Poke

## First steps

### Installing node
Get the version of node specified in the [.nvmrc](.nvmrc) file from the [official website](https://nodejs.org/) or using [nvm](https://github.com/creationix/nvm)
Nvm approach is preferred.

### Installing docker & docker-compose
Get the latest version of docker & docker-compose. Read the [Docker Compose Guide](https://docs.docker.com/compose/install/) for info and links on how to do it.

### Env variables
Create a `.env` file with the variables listed in the `.env.sample` file

Although those variables values are OK for local development, they might be different for each non-local environment.

## Running the server
Once everyting is installed and configured, from the root directory, run ```docker-compose up```. See the [Known Issues](#Known-Issues) section if a problem arises.
Remember to set the `NPM_RUN` variable in the `.env` file for development or non-development mode.

You should be ready to ```curl``` the server by `localhost:3000`.
Check the [routes.js](./app/routes.js) file for the available routes.

## Development
1. Run ```npm i``` from rootpath of the project to get all the dependencies.
2. Run ```npm i``` from the docker container for the node app each time you install a new dependency.
3. Run ```npm run migration:run``` from the docker container for the node app each time you create a new migration (if you do not want to restart the container).
4. Open the root directory in your favourite IDE/Editor.
5. Start to code!

### Test
To run the tests, enter the node app docker container and from there, run ```npm run test```.

### Populate ES Index
Once the enviornment is set up, you have to run a POST call to the `/pokemons/index` endpoint using the defined API key. For example:

    curl --location --request POST 'http://localhost:3000/pokemons/index' \
    --header 'X-API-KEY: SampleApiKey'

This will load all the Pokemons names (with their URL) from the PokeAPI into Elasticsearch.

### Connecting to the docker containers
#### Node
1. Find the docker container id using the `docker ps` command. Copy it.
2. Enter the container by running `docker exec -it <container_id> /bin/bash`.

For a shortcut you can use directly the command below:

      docker exec -it `docker ps | grep pokeapi-test-node_api_1 | cut -d ' ' -f1` /bin/bash

## Known Issues
- [ ]

## Docs
You may find more information related to this project in the [docs](./docs) directory.
