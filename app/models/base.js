const { Model } = require('objection');
const knex = require('../../knex');

// Give the knex instance to objection.
Model.knex(knex);

exports.Model = Model;
