const apiKeyMiddleware = require('./middlewares/api_key');
const pokemonsController = require('./controllers/pokemons');

const load = app => {
  app.get('/', [], pokemonsController.view);
  app.get('/search', [], pokemonsController.search);
  app.post('/pokemons/index', [apiKeyMiddleware.validateApiKey], pokemonsController.index);
};

exports.load = load;
