const config = require('../../config/index');
const errors = require('../errors');

const validateApiKey = (req, _res, next) => {
  const apiKey = req.headers[config.headers.apiKey];
  const error = apiKey === config.headers.apiKeyValue ? undefined : errors.invalidApiKey;
  next(error);
};

exports.validateApiKey = validateApiKey;
