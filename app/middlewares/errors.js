const moment = require('moment');

const errorInternalCodes = require('../errors/internal_code');
const logger = require('../logger');

const DEFAULT_STATUS_CODE = 500;

const statusCodes = {
  [errorInternalCodes.INVALID_API_KEY]: 400
};

const handle = (error, _, res, next) => {
  if (error.internalCode) res.status(statusCodes[error.internalCode] || DEFAULT_STATUS_CODE);
  else {
    next(error);
    res.status(DEFAULT_STATUS_CODE);
    error.errors = { code: '1000', message: error.message };
  }
  logger.error(error);
  return res.send({ errors: error.errors, timestamp: moment().format() });
};

exports.handle = handle;
