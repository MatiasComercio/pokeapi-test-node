const axios = require('axios');
const logger = require('../logger');

const BASE_URL = 'https://pokeapi.co/api/v2';
// TODO: just for future reference to dinamically define what fields to index
// const SCHEMAS_BASE_URL = 'https://github.com/PokeAPI/api-data/tree/d4efc28d30b8ca7258b0717f475623fada5e0304/data/schema/v2';

const instance = axios.create({
  baseURL: BASE_URL,
  timeout: 10000
});

instance.interceptors.request.use(request => {
  logger.debug('Starting Request', request);
  return request;
});

instance.interceptors.response.use(response => {
  logger.debug('Response:', response);
  return response;
});

module.exports = instance;
