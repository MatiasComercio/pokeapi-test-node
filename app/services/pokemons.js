const bb = require('bodybuilder');
const esClient = require('../../elasticsearch');
const pokeapiService = require('./pokeapi');
const logger = require('../logger');

const POKEMONS_PATH = '/pokemon';
const PAGE_SIZE = 1000;
const INDEX_NAME = 'pokemons';

const recreateIndex = async () => {
  const params = {
    index: INDEX_NAME
  };

  if (await esClient.indices.exists(params)) {
    await esClient.indices.delete(params);
  }

  await esClient.indices.create(
    {
      body: {
        mappings: {
          properties: {
            id: { type: 'text' },
            name: { type: 'text' },
            url: { type: 'text' }
          }
        }
      },
      ...params
    },
    { ignore: [400] }
  );
};

const rawFetchPokemons = params => pokeapiService.get(POKEMONS_PATH, { params });

const fetchPokemons = async params => {
  const response = await rawFetchPokemons(params);
  return response.data.results;
};

const fetchPokemonCount = async () => {
  const response = await rawFetchPokemons({ limit: 0, offset: 0 });
  return response.data.count;
};

const handleIndexErrors = (body, bulkResponse) => {
  const erroredDocuments = [];
  // The items array has the same order of the dataset we just indexed.
  // The presence of the `error` key indicates that the operation
  // that we did for the document has failed.
  bulkResponse.items.forEach((action, i) => {
    const operation = Object.keys(action)[0];
    if (action[operation].error) {
      const index = i * 2;
      erroredDocuments.push({
        // If the status is 429 it means that you can retry the document,
        // otherwise it's very likely a mapping error, and you should
        // fix the document before to try it again.
        status: action[operation].status,
        error: action[operation].error,
        operation: body[index],
        document: body[index + 1]
      });
    }
  });

  logger.error(erroredDocuments);
};

const indexPokemons = async pokemons => {
  const body = pokemons.flatMap(pokemon => {
    const doc = {
      // Using `name` as `id` for now.
      id: pokemon.name,
      name: pokemon.name,
      url: pokemon.url
    };

    return [{ index: { _index: INDEX_NAME } }, doc];
  });

  const bulkResponse = await esClient.bulk({ refresh: true, body });

  if (bulkResponse.errors) {
    handleIndexErrors(body, bulkResponse);
  }

  const response = await esClient.count({ index: INDEX_NAME });
  logger.info(`Indexed # ${response.count} pokemons`);
};

const indexPage = async (offset, pageSize) => {
  const pokemons = await fetchPokemons({ offset, limit: pageSize });
  await indexPokemons(pokemons);
};

const index = async () => {
  await recreateIndex();

  const count = await fetchPokemonCount();

  for (let offset = 0; offset < count; offset += PAGE_SIZE) {
    await indexPage(offset, PAGE_SIZE);
  }
};

const search = async queryName => {
  const queryBody = bb()
    .query('wildcard', 'name', `${queryName}*`)
    .build();

  try {
    logger.info(queryBody);
    const results = await esClient.search({ index: INDEX_NAME, body: queryBody });
    return results.hits.hits;
  } catch (err) {
    logger.error(err);
    return [];
  }
};

exports.index = index;
exports.search = search;
