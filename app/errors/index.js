const codes = require('./code');
const internalCodes = require('./internal_code');
const messages = require('./message');

const buildError = ({ code, message, internalCode }) => {
  const errors = [{ message, code }];
  return { errors, internalCode };
};

const invalidApiKey = buildError({
  code: codes.INVALID_API_KEY,
  internalCode: internalCodes.INVALID_API_KEY,
  message: messages.INVALID_API_KEY
});

exports.invalidApiKey = invalidApiKey;
