const path = require('path');
const pokemonsService = require('../services/pokemons');

const view = (_req, res) => {
  res.sendFile('template.html', {
    root: path.join(__dirname, '../../views')
  });
};

const search = async (req, res) => {
  const queryParam = req.query.q;
  const pokemons = await pokemonsService.search(queryParam);
  res.send(pokemons);
};

const index = async (_req, res) => {
  await pokemonsService.index();
  res.sendStatus(200);
};

exports.view = view;
exports.search = search;
exports.index = index;
